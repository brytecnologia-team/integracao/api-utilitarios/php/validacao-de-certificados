# Validação de Certificado

Este é um exemplo de integração dos serviços da API de verificação com clientes baseados em tecnologia PHP. 

Este exemplo apresenta os passos necessários para a validação de um certificado.
  - Passo 1: Carrega o certificado a ser validado.
  - Passo 2: Envia a requisição para o serviço da BRy e imprime todos os valores referentes ao certificado e sua validade.


### Tech

O exemplo utiliza das bibliotecas PHP abaixo:
* [PHP 7.4] - PHP is a popular general-purpose scripting language that is especially suited to web development.
* [CURL] - Client URL Library.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Para utilizar o serviço como SaaS, a url base que deve ser configurada é https://fw2.bry.com.br. Para implementações On Premise, o valor da url base deve ser igual ao valor da url do HUB Signer.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| caminhoDoCertificado | Localização do certificado a ser verificado. | ValidadorCertificado.php
| token | Access Token para o consumo do serviço (JWT). | ValidadorCertificado.php

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo é necessário ter o PHP 7.x instalado na sua máquina, além de instalar a biblioteca php-curl.

Comandos:

Instalar php-curl (linux):

    -apt-get install php-curl

Caso o seu sistema operacional seja outro, verifique sobre a instalação [aqui](https://www.php.net/manual/pt_BR/curl.requirements.php).

Executar programa:

    -php ValidadorCertificado.php

   [PHP 7.4]: <https://www.php.net/releases/7_4_0.php>
   [CURL]: <https://www.php.net/manual/pt_BR/book.curl.php>
