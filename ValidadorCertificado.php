<?php 

// URL PARA A VALIDAÇÃO DE CERTIFICADOS
const URLValidacao = "https://fw2.bry.com.br/api/validador-service/v1/certificateReports";
//https://URL_HUB_SIGNER/api/validador-service/v1/certificateReports
// CAMINHO ONDE ESTÁ LOCALIZADO O CERTIFICADO
const caminhoDoCertificado = '/caminho/para/o/certificado.cer';
// TOKEN AUTHORIZATION GERADO NO BRY CLOUD
const token = "tokenDeAutenticação";

function validarCertificado() {

    echo "===================== Inicializando Validação de Certificado ===================";
    echo "\n\n__________________________________________________\n\n";

    $certificado = file_get_contents(caminhoDoCertificado);
    $certificadoB64 = base64_encode($certificado);
    $certificadoStr = utf8_decode($certificadoB64);

    // CRIA O JSON QUE SERÁ ENVIADO NA REQUISIÇÃO DE VALIDAÇÃO DE CERTIFICADO
    $json = '{"nonce": "1", "mode": "CHAIN",
            "contentReturn": "true",
            "extensionsReturn": "true", 
            "certificates": [{"content": "' . $certificadoStr . '", 
            "nonce": "1"}]}';

    // CRIA A REQUISIÇÃO DA VALIDAÇÃO DE CERTIFICADO
    $curlValidacao = curl_init();

    curl_setopt_array($curlValidacao, array(
    CURLOPT_URL => URLValidacao,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS =>$json,
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer " . token
    ),
));

    // ENVIA A REQUISIÇÃO PARA O BRY FRAMEWORK
    $respostaValidacao = curl_exec($curlValidacao);
    $httpcode = curl_getinfo($curlValidacao, CURLINFO_HTTP_CODE);
    $respostaJson = json_decode($respostaValidacao);
    curl_close($curlValidacao);

    // SE A REQUISIÇÃO FOI FEITA COM SUCESSO, IMPRIME OS VALORES DO CERTIFICADO
    if($httpcode == 200) {
        $statusDaCadeiaDeCertificados = $respostaJson->reports[0]->status;
        $indexCertificado = count($statusDaCadeiaDeCertificados->certificateStatusList) - 1;
        $titularDoCertificado = $statusDaCadeiaDeCertificados->certificateStatusList[$indexCertificado];

        // IMPRIME A RESPOSTA DA VALIDAÇÃO
        echo "Resposta JSON da finalização:\n\n";
        echo $respostaValidacao;
        echo "\n\n__________________________________________________\n\n";
        
        if($statusDaCadeiaDeCertificados != null && $titularDoCertificado != null) {
            echo "Nome: " . $titularDoCertificado->certificateInfo->subjectDN->cn . "\n";
            echo "Status geral da cadeia de certificado: " . $statusDaCadeiaDeCertificados->status . "\n";
            echo "Status do Certificado: " . $titularDoCertificado->status . "\n";

            if($titularDoCertificado->status == 'INVALID') {
                echo $titularDoCertificado->errorStatus . "\n";
                if($titularDoCertificado->revocationStatus != null) {
                    echo $titularDoCertificado->revocationStatus . "\n";
                }
            } 

            echo $titularDoCertificado->pkiBrazil;
            echo "Data de validade inicial do certificado: " . $titularDoCertificado->certificateInfo->validity->notBefore . "\n";
            echo "Data de validade final do certificado: " . $titularDoCertificado->certificateInfo->validity->notAfter . "\n";
        } else {
            echo "Incomplete chain of the signatory: It was not possible to verify the certificate" . "\n";
            echo "Overall chain status " . $statusDaCadeiaDeCertificados->status . "\n";
        }
        } else { 
            echo "Erro ao fazer requisição: " . $respostaValidacao;
        }
}

validarCertificado();

?>